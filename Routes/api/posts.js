const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const auth = require('../../middleware/auth');

const Posts = require('../../models/Posts');
const Users = require('../../models/Users');


// @route    POST api/posts
// @desc     Create a post
// @access   Private
router.post(
  '/',
  [auth, [check('text', 'Text is required').not().isEmpty()]],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const user = await User.findById(req.user.id).select('-password');

      const newPost = new Posts({
        text: req.body.text,
        name: user.name,
        avatar: user.avatar,
        user: req.user.id
      });

      const posts = await newPost.save();

      res.json(posts);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route    GET api/posts
// @desc     Get all posts
// @access   Private
router.get('/', auth, async (req, res) => {
  try {
    const posts = await Posts.find().sort({ date: -1 });
    res.json(posts);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route    GET api/posts/:id
// @desc     Get post by ID
// @access   Private
router.get('/:id', auth,  async (req, res) => {
  try {
    const posts = await Posts.findById(req.params.id);

    res.json(posts);
  } catch (err) {
    console.error(err.message);
     if(err.kind == 'ObjectID') {
    	return res.status(404).json({ msg: 'Post not Found'});
    }
    res.status(500).send('Server Error');
  }
});

// @route    DELETE api/posts/:id
// @desc     Delete a post
// @access   Private
router.delete('/:id', auth,  async (req, res) => {
  try {
    const posts = await Posts.findById(req.params.id);

    if (!posts) {
      return res.status(404).json({ msg: 'Post not found' });
    }

    // Check user
    if (posts.user.toString() !== req.user.id) {
      return res.status(401).json({ msg: 'User not authorized' });
    }

    await posts.remove();

    res.json({ msg: 'Post removed' });
  } catch (err) {
    console.error(err.message);
  if(err.kind == 'ObjectID') {
    	return res.status(404).json({ msg: 'Post not Found'});
    }
    res.status(500).send('Server Error');
  }
});

// @route    PUT api/posts/like/:id
// @desc     Like a post
// @access   Private
router.put('/like/:id', auth,  async (req, res) => {
  try {
    const posts = await Posts.findById(req.params.id);

    // Check if the post has already been liked
    if (posts.likes.filter(like => like.user.toString() === req.user.id).length>0) {
      return res.status(400).json({ msg: 'Post already liked' });
    }

    posts.likes.unshift({ user: req.user.id });

    await posts.save();

    return res.json(posts.likes);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route    PUT api/posts/unlike/:id
// @desc     Unlike a post
// @access   Private
router.put('/unlike/:id', auth, async (req, res) => {
  try {
    const posts = await Posts.findById(req.params.id);

    // Check if the post has not yet been liked
    if (posts.likes.filter(like => like.user.toString() === req.user.id).length ===0) {
      return res.status(400).json({ msg: 'Post has not yet been liked' });
    }

  //Get remove index
  const removeIndex = posts.likes.map(like => like.user.toString()).indexOf(req.user.id);

  posts.likes.splice(removeIndex, 1);

    await posts.save();

    return res.json(posts.likes);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route    POST api/posts/comment/:id
// @desc     Comment on a post
// @access   Private
router.post(
  '/comment/:id',
  [
    auth,
 
    check('text', 'Text is required').not().isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const user = await User.findById(req.user.id).select('-password');
      const posts = await Posts.findById(req.params.id);

      const newComment = {
        text: req.body.text,
        name: user.name,
        avatar: user.avatar,
        user: req.user.id
      };

      posts.comments.unshift(newComment);

      await posts.save();

      res.json(posts.comments);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route    DELETE api/posts/comment/:id/:comment_id
// @desc     Delete comment
// @access   Private
router.delete('/comment/:id/:comment_id', auth, async (req, res) => {
  try {
    const posts = await Posts.findById(req.params.id);

    // Pull out comment
    const comment = posts.comments.find(
      comment => comment.id === req.params.comment_id
    );
    // Make sure comment exists
    if (!comment) {
      return res.status(404).json({ msg: 'Comment does not exist' });
    }
    // Check user
    if (comment.user.toString() !== req.user.id) {
      return res.status(401).json({ msg: 'User not authorized' });
    }

    //Get remove index
  const removeIndex = posts.comments.map(comment => comment.user.toString()).indexOf(req.user.id);

  posts.comments.splice(removeIndex, 1);

    await posts.save();

    return res.json(posts.comments);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
    
});

module.exports = router;